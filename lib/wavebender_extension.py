#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  wavebender_extension.py -> adds frame-generator to wavebender
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import sys
import wave
import math
import struct
import random
import argparse
from itertools import *

def compute_samples2(channels, nsamples=None, offset=0):
	return islice(izip(*(imap(sum, izip(*channel)) for channel in channels)), offset, offset+nsamples)

def generate_frames(samples,sampwidth=2):
	max_amplitude = float(int((2 ** (sampwidth * 8)) / 2) - 1)
	frames=''
	for s in samples:
		for sc in s: # channels!
			frames+=struct.pack('h', int(max_amplitude*sc))


	#frames = ''.join(''.join(struct.pack('h', int(max_amplitude * sample)) for sample in channels) for channels in samples if channels is not None)
	return frames

def append_to_frames(existing_frames,new_samples):
	new_frames=generate_frames(new_samples)
	return ''.join((existing_frames,new_frames))

def write_prepared_frames_to_wavfile(f, frames, nframes=None, nchannels=2, sampwidth=2, framerate=44100):
	"Write prepared frames to a wavefile."
	if nframes is None:
		nframes = -1
	w = wave.open(f, 'w')
	w.setparams((nchannels, sampwidth, framerate, nframes, 'NONE', 'not compressed'))
	w.writeframesraw(frames)
	w.close

#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  nomatim (reverse) osm geocoder
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

import urllib2, json

def geocode(placename):
	nomatim=urllib2.urlopen("http://nominatim.openstreetmap.org/search?format=json&q=%s" %(placename))
#	nomatim=urllib2.urlopen("http://nominatim.openstreetmap.org/search?format=json&city=%s" %(placename))  # we work with cities only, anyway
	places=json.loads(nomatim.read())
	nomatim.close()

	#print places
	
	if len(places)<1:
		print "Could not find a place named '%s'" %(placename,)
		return False

	return (
		float(places[0][u'lat']),
		float(places[0][u'lon'])
	)

def main():
	import argparse
        parser = argparse.ArgumentParser(description='Geocode place names via nomatim')
        parser.add_argument('place')
        args = vars(parser.parse_args())

	print geocode(args['place'])
	pass

if __name__ == '__main__':
        main()

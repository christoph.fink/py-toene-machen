#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  musical-notes.py -> define frequencies of musical notes
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

class MusicalNote:
	"""
	musical note, featuring engl and german names, and frequencies
	"""
	def __init__(self,name_de="none",name_en="none",frequency=0):
		self.name_de=str(name_de)
		self.name_en=str(name_en)
		self.frequency=float(frequency)

	def __repr__(self):
		return "Musical note %s (%s), freqency %.2f Hz" %(self.name_en, self.name_de,self.frequency)

a_=MusicalNote("a'","A4",440)

notes=[]

for octave in range(8):
	for note in ((0,"A","a"),(2,"B","h"),(3,"C","c"),(5,"D","d"),(7,"E","e"),(8,"F","f"),(10,"G","g")):
		(o,e,d)=note
		name_en="%s%d" %(e,octave)
		if octave<4:
			name_de="%s%s" %((2-octave)*',',d.upper())
		elif octave>4:
			name_de="%s%s" %(d.lower(),(octave-2)*"'")
		else:
			name_de=d.lower()

		n=((octave-4.0)*12.0)+o  # distance from A4/a'
		frequency=(2**(n/12.0))*440.0
		
		notes.append(MusicalNote(name_de,name_en,frequency))
		

def main():
	print notes

if __name__ == '__main__':
        main()
